"""
В условии задачи не сказано, как ведут себя крайние и угловые клетки, не имеющие восьми соседей.
Можно сделать следующие допущения:
1. Такие клетки никак не взаимодействуют с другими, т.е. остаются неизменными.
2. Взаимодействуют с соседями, которых может быть от 3 до 5.

1) Предположим, что принято 1-е допущение и клетки, имеющие число соседей меньше 8, свои значения не меняют.
Реализовано в функции surv_process().

2) Во втором случае можно воспользоваться тем же алгоритмом, что и в surv_process(), расширив исходную матрицу
краевыми столбцами и строками с нулевыми элементами. Получить матрицу сумм соседних клеток и сравнить по условию задачи.
В библиотеке Scipy уже есть подобный алгоритм свертки: нужно только задать фильтр для подсчета "соседей".
Реализовано в функции surv_process_2().

Цикл завершается, если доска перестает изменяться, либо прерывается пользователем (Ctrl + C).
"""

import numpy as np
import scipy.signal
import time


def init_random(m,n):
    """ Генерация матрицы размера m x n со значениями 0 и 1. """
    return np.random.randint(0,2,(m,n))


def init_file(m,n):
    """ Файл содержит начальные координаты (индексы матрицы) живых клеток.
        Первая строка в файле - индексы i,
        вторая строка - соответствующие им индексы j. """
    with open('init_file.txt') as f:
        m_init = []
        for line in f.readlines():
            m_init.append([int(i) for i in line.strip().split(',')])
    if len(m_init[0]) > m*n or len(m_init[0]) > m*n:
        raise ValueError('Количество начальных живых клеток превышает общее число клеток доски.')
    if max(m_init[0]) >= m or max(m_init[1]) >= n:
        raise IndexError('Индексы в файле превышают индексы заданной матрицы.')
    if len(m_init[0]) != len(m_init[1]):
        raise ValueError('Количество i и j индексов в файле не совпадает.')

    print('Координаты живых клеток в начальном состоянии:', m_init)
    return tuple(m_init)


def surv_process(d):
    """ Суммируем значения соседних клеток и сравниваем по заданным условиям. """
    m, n = d.shape
    changed_board = d.copy()
    for i in range(0, m):
        for j in range(0, n):
            if 0 < i <= m-2 and 0 < j <= n-2:
                s = d[i-1:i+2, j-1:j+2].sum() - d[i,j]
                if s < 2 or s > 3:
                    changed_board[i,j] = 0
                elif s == 3:
                    changed_board[i,j] = 1
    return changed_board


def surv_process_2(d):
    """ С учетом угловых и краевых клеток. """
    m, n = d.shape
    changed_board = d.copy()
    f = np.array([[1,1,1], [1,0,1], [1,1,1]]) # выбираем 8 соседей для подсчета
    s = scipy.signal.convolve2d(d, f, mode='same')
    #print('----Матрица сумм соседних клеток----')
    #print(s)
    for i in range(0, m):
        for j in range(0, n):
            if s[i,j] < 2 or s[i,j] > 3:
                changed_board[i,j] = 0
            elif s[i,j] == 3:
                changed_board[i,j] = 1
    return changed_board


def main():
    print('Задайте размеры доски (m x n)')
    while True:
        try:
            m, n = int(input('Введите m: ')), int(input('Введите n: '))
            if m <= 0 or n <= 0:
                print('Введите целое положительное число!')
                continue
        except:
            print('Введите целое положительное число!')
            continue
        break
    while True:
        start = input('Выберите способ задания начального состояния (r-случайно, f-из файла): ').lower()
        if start == 'r':
            board = init_random(m,n)
            break
        elif start == 'f':
            board = np.zeros([m,n])
            board[init_file(m,n)] = 1
            break
        else:
            continue

    print('---Начальное состояние---')
    print(board)
    prev_board = np.array([])
    while True:
        board = surv_process_2(board);
        print('-------------------------')
        print(board)
        if np.array_equal(prev_board, board):
            print('Процесс завершен!')
            break
        prev_board = board.copy()
        time.sleep(1)


if __name__ == '__main__':
    main()
